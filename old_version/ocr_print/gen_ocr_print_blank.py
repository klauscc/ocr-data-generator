#coding=utf-8
#python3
import os
import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from skimage.transform import rotate
import skimage

class font_generator():
    def __init__(self, root_dir):
        self.font_list, self.fs_list, self.charset_list = self.load_font(root_dir)
        self.font_size = len(self.font_list)

    def get_font(self):
        index = np.random.randint(0, self.font_size)
        return self.font_list[index], self.fs_list[index], self.charset_list[index]

    def get_font_index(self, index):
        return self.font_list[index], self.fs_list[index], self.charset_list[index]

    def load_font(self, root_dir):
        font_list = []
        fs_list = []
        charset_list = []
        font_size = list(range(15,31))
        ttf_dir = os.path.join(root_dir, 'ttf')
        txt_dir = os.path.join(root_dir, 'txt')
        for root, dirs, files in os.walk(ttf_dir):
            for filename in files:
                suffix = os.path.splitext(filename)[1]
                if suffix == '.ttf' or suffix == '.TTF' or suffix == '.otf' or suffix == '.ttc':
                    name = os.path.splitext(filename)[0]
                    txt_name = os.path.join(txt_dir, name+'.txt')
                    with open(txt_name) as f_charset:
                        charset = f_charset.read()
                    for fs in font_size:
                        font_list.append(ImageFont.truetype(os.path.join(ttf_dir, filename), fs, 0))
                        fs_list.append(fs)
                        charset_list.append(charset)
        return font_list, fs_list, charset_list

class background_generator():
    def __init__(self, root_dir, dir_list):
        self.bg_list = self.load_bg(root_dir, dir_list)
        self.bg_size = len(self.bg_list)

    def get_bg(self):
        index = np.random.randint(0, self.bg_size)
        return self.bg_list[index]

    def load_bg(self, root_dir, dir_list):
        bg_list = []
        for d in dir_list:
            path = os.path.join(root_dir, d)
            for root, dirs, files in os.walk(path):
                for filename in files:
                    if os.path.splitext(filename)[1] == '.jpg':
                        image = cv2.imread(os.path.join(path, filename))
                        bg_list.append(image)
        return bg_list

class color_generator():
    def __init__(self):
        self.black = (0, 0, 0)
        self.blue = (255, 0, 0)
        self.red = (0, 0, 255)
        self.white = (255, 255, 255)

    def get_color(self, type = 0):
        if type == 1:
            return -1, self.white
        rint = np.random.randint(0,10)
        if rint == 0:
            return 0, self.red
        elif rint == 1:
            return 1, self.blue
        else:
            return 2, self.black 

class text_generator():
    def __init__(self, filename):
        self.name_text_list = self.load_text(filename)
        self.list_size = len(self.name_text_list)
        self.cnt = 0

    def get_text(self):
        if self.cnt < self.list_size:
            name_text = self.name_text_list[self.cnt]
            self.cnt += 1
            return self.cnt - 1, name_text.split()
        else:
            return -1, ['', '']

    def load_text(self, filename):
        f_read = open(filename, 'r')
        line = f_read.readline()
        name_text_list = []
        while line:
            name_text_list.append(line[0:len(line)-1])
            line = f_read.readline()
        f_read.close()
        name_text_list = sorted(name_text_list, key=lambda c : len(c))
        return name_text_list

def add_blank(text, num):
    ret_text = ''
    for i in range(len(text)):
        ret_text += text[i]
        if i != len(text)-1:
            k = num
            if num > 2:
                k = np.random.randint(1, num+1)
            for j in range(k):
                ret_text += ' '
    return ret_text

def add_noise_text(img, noise, type):
    for j in range(img.shape[0]):
        for i in range(img.shape[1]):
            if (type == 0 and img[j][i][0] > 128 and img[j][i][1] < 128 and img[j][i][2] < 128) \
            or (type == 1 and img[j][i][0] < 128 and img[j][i][1] < 128 and img[j][i][2] > 128) \
            or (type == 2 and img[j][i][0] < 128 and img[j][i][1] < 128 and img[j][i][2] < 128) :
                for k in range(3):
                    n = np.random.randint(0, noise) - int(noise) / 2
                    if n < 0 and abs(n) > img[j][i][k]:
                        img[j][i][k] = 0
                    else:
                        img[j][i][k] += n
    return img

def add_noise_all(img, size):
    if size <= 20:
        size = 20
    else:
        size = np.random.randint(20, size)
    for i in range(20):
        temp_x = np.random.randint(0, img.shape[0])
        temp_y = np.random.randint(0, img.shape[1])
        temp_r = np.random.randint(0, 255)
        temp_g = np.random.randint(0, 255)
        temp_b = np.random.randint(0, 255)
        img[temp_x][temp_y][0] = temp_r
        img[temp_x][temp_y][1] = temp_g
        img[temp_x][temp_y][2] = temp_b
    return img

def add_erode(img, size):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size, size))    
    img = cv2.erode(img, kernel) 
    return img

def add_dilate(img, size):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size, size))    
    img = cv2.dilate(img, kernel) 
    return img

def add_rotate(img, angle):
    img = rotate(img, angle, resize=False, mode='reflect')
    img = skimage.img_as_ubyte(img)
    return img

def gen_text_image(text, bg_img, color, font):
    font_width, font_height = font.getsize(text)
    expand_height = np.random.randint(0, int(font_height * 0.6))
    expand_width = np.random.randint(0, int(font_height))
    height = font_height + expand_height
    width = font_width + expand_width
    bg_img = np.resize(bg_img, [height, width, 3])
    img_obj = Image.fromarray(bg_img)
    draw = ImageDraw.Draw(img_obj)
    if expand_width > 0:
        x = np.random.randint(0, expand_width)
    else:
        x = 0
    if expand_height > 0:
        y = np.random.randint(0, expand_height)
    else:
        y = 0
    draw.text((x, y), text, fill=color, font=font)
    img = np.array(img_obj)
    return img, bg_img

def check_font_valid(charset, text):
    for t in text:
        if t not in charset:
            return False
    return True

def load_text(filename):
    f_read = open(filename, 'r')
    text = f_read.read()
    f_read.close()
    return text

def main():
    font_gen = font_generator('all_fonts')
    bg_gen_shallow = background_generator('background', ['clean', 'dirty', 'hyperclean'])
    bg_gen_dark = background_generator('background', ['dark'])
    color_gen = color_generator()
    text_gen = text_generator('data1/images_blank.txt')
    root_dir = 'data1/images_blank'

    cnt = 0
    while 1:
        cnt, [filename, text] = text_gen.get_text()
        print(cnt, text)
        if cnt == -1:
            break

        if len(text) > 1:
            r_blank = np.random.randint(0, 3)
            if r_blank == 0:
                text = add_blank(text, 1)
            elif r_blank == 1:
                text = add_blank(text, 2)
            elif r_blank == 2:
                text = add_blank(text, 3)
        else:
            continue

        r_bg = np.random.randint(0, 10)
        if r_bg == 0:
            bg_img = bg_gen_dark.get_bg()
        else:
            bg_img = bg_gen_shallow.get_bg()
        r_rotate_bg = np.random.randint(0, 5)
        if r_rotate_bg == 0:
            angle = np.random.rand() * 10 - 5
            bg_img = add_rotate(bg_img, angle)

        if r_bg == 0:
            color_type, color = color_gen.get_color(1)
        else:
            color_type, color = color_gen.get_color()

        font, fs, charset = font_gen.get_font()
        cnt1 = 0
        while not check_font_valid(charset, text):
            font, fs, charset = font_gen.get_font()
            cnt1 += 1
            if cnt1 > 20:
                break

        image, bg_img = gen_text_image(text, bg_img, color,font)

        r_rotate = np.random.randint(0, 5)
        if r_rotate == 0:
            angle = np.random.rand() * 4 - 2.0
            image = add_rotate(image, angle)

        font_width, font_height = font.getsize(text)
        if font_height > 30:
            erode_size = np.random.randint(2,4)
            dilate_size = np.random.randint(2,4)
        else:
            erode_size = 2
            dilate_size = 2

        r_noise_all = np.random.randint(0, 5)
        if r_noise_all == 0:
            image = add_noise_all(image, int(0.03 * font_height * font_width))

        r_noise_text = np.random.randint(0, 5)
        if r_noise_text == 0:
            image = add_noise_text(image, 128, color_type)

        r_erode = np.random.randint(0, 10)
        if r_erode == 0:
            image = add_erode(image, erode_size)

        r_dilate = np.random.randint(0, 10)
        if r_erode == 0 and r_dilate == 0:
            image = add_dilate(image, dilate_size)

        cv2.imwrite(os.path.join(root_dir, filename), image)
    
if __name__=='__main__':
    main()
