import os
import random
import numpy as np

chars1 = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,;:/(){}?'
chars2 = 'ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ０１２３４５６７８９．，；：／（）｛｝？'

def load_dict_count(name):
    dict_count = {}
    f_count = open(name)
    line = f_count.readline()
    while line:
        d = {}
        lines = line.split(' ')
        name = lines[0]
        context = lines[1:]
        for c in context:
            cs = c.split(',')
            d[int(cs[0])] = int(cs[1])
        dict_count[name] = d
        line = f_count.readline()
    f_count.close()
    return dict_count

def load_dict_count_1(name, num):
    dict_count = {}
    f_count = open(name)
    line = f_count.readline()
    while line:
        d = {}
        for i in range(1,num+1):
            d[i] = 0
        lines = line.split(' ')
        name = lines[0]
        context = lines[1:]
        for c in context:
            cs = c.split(',')
            if int(cs[0]) <= num:
                d[int(cs[0])] = int(cs[1])
        dict_count[name] = d
        line = f_count.readline()
    f_count.close()
    return dict_count

def convert_num_labels(num_labels, name_list, min_len, max_len):
    ret_num_labels = {}
    for name in name_list:
        d = {}
        for j in range(min_len,max_len+1):
            d[j] = num_labels[j][name]
        ret_num_labels[name] = d
    return ret_num_labels

def get_label_nums(name_list, num_list, dict_count, num_charset, min_len, max_len, num_sets=100000):
    dict_nums = {}
    for j in range(min_len,max_len+1):
        total_num = 0
        d_nums = {}
        for i in range(len(name_list)):
            name = name_list[i]
            d_nums[name] = 0
        dict_nums[j] = d_nums

        for i in range(len(name_list)):
            name = name_list[i]
            num_real = dict_count[name][j]
            num_set = int(num_list[i] / 10)
            num = 0
            if num_set > 0 and num_real > num_set:
                num = num_set
            else:
                num = num_real
            dict_nums[j][name] = num
            total_num += num
        dict_nums[j]['total'] = total_num

    dict_nums[1]['total'] += num_charset

    for j in range(min_len,max_len+1):
        num_res = num_sets - dict_nums[j]['total']
        if dict_count['texts.txt'][j] - dict_nums[j]['texts.txt'] > num_res:
            dict_nums[j]['texts.txt'] += num_res
            dict_nums[j]['total'] = num_sets
        else:
            num = dict_count['texts.txt'][j] - dict_nums[j]['texts.txt']
            dict_nums[j]['texts.txt'] += num
            dict_nums[j]['total'] += num

    total_num = 0
    for j in range(min_len,max_len+1):
        for i in range(len(name_list)):
            name = name_list[i]
            print(name, str(dict_nums[j][name]) + ' ')
        total_num += dict_nums[j]['total']
        print('total', str(dict_nums[j]['total']) + '\n')
    print('total', str(total_num) + '\n')
    return convert_num_labels(dict_nums, name_list, min_len, max_len)

def filter(charset, text):
    label = ''
    text_clear = ''
    find = False
    for i in range(len(text)):
        if text[i] in chars2:
            pos = chars2.find(text[i])
            if chars1[pos] in charset:
                label += chars1[pos]
                text_clear += text[i]
            find = True
        else:
            if text[i] in charset:
                label += text[i]
                text_clear += text[i]

    return text_clear, label

def gen_spacial_text(num, length, type):
    if type == 0:
        charset = '0123456789.—:'
    elif type == 1:
        charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.:—'
    else:
        charset = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.:—'
    text_list = []
    for j in range(num):
        text = ''
        for i in range(length):
            r_index = np.random.randint(0, len(charset))
            text += charset[r_index]
        text_list.append(text)
    return text_list

if __name__=='__main__':
    src_dir = 'materials_4pd'
    name_list = ['address.txt', 'company_name.txt', 'people_name.txt', 'texts.txt', 'upper_numbers.txt']
    num_list = [80000, 80000, 40000, 200000, -1]
    spacial_num = 40000
    min_len = 1
    max_len = 30
    #dict_count = load_dict_count('materials_4pd_count.txt')
    dict_count = load_dict_count_1('materials_4pd_count.txt', max_len)
    with open('charset_4pd.txt') as f_charset:
        charset = f_charset.read()

    dict_nums = get_label_nums(name_list, num_list, dict_count, len(charset), min_len, max_len)
    print(dict_nums)

    dict_nums_cnt = {}
    for name in name_list:
        d = {}
        for j in range(min_len,max_len+1):
            d[j] = 0
        dict_nums_cnt[name] = d
    print(dict_nums_cnt)

    f_train = open('data1/train.txt', 'w')
    f_val = open('data1/val.txt', 'w')
    f_test = open('data1/test.txt', 'w')
    f_image = open('data1/images.txt', 'w')
    cnt = 0
    
    for name in name_list:
        f_read = open(os.path.join(src_dir, name))
        line = f_read.readline()
        while line:
            line = line.strip()
            l = len(line)
            if l >= min_len and l <= max_len:
                if dict_nums_cnt[name][l] < dict_nums[name][l]:
                    image_name = name[:-4] + '_' + str(l).zfill(2) + '_' + str(dict_nums_cnt[name][l]).zfill(6) + '.jpg'
                    text, label = filter(charset, line)
                    print(text)
                    f_image.write(image_name + ' ' + text + '\n')
                    if cnt % 10 == 8:
                        f_val.write(image_name + ' ' + label + '\n')
                    elif cnt % 10 == 9:
                        f_test.write(image_name + ' ' + label + '\n')
                    else:
                        f_train.write(image_name + ' ' + label + '\n')
                    cnt += 1

                    dict_nums_cnt[name][l] += 1
            line = f_read.readline()
        f_read.close()
    
    dict_name = {0:'num', 1:'alphabet', 2:'num_alphabet'}
    cnt = 0
    for k in range(min_len, max_len+1):
        for j in range(3):
            spacial_list = gen_spacial_text(spacial_num // 30, k, j)
            for i in range(len(spacial_list)):
                text = spacial_list[i]
                print(text)
                image_name = dict_name[j] + '_' + str(k).zfill(2) + '_' + str(i).zfill(6) + '.jpg'
                f_image.write(image_name + ' ' + text + '\n')
                if cnt % 10 == 8:
                    f_val.write(image_name + ' ' + text + '\n')
                elif cnt % 10 == 9:
                    f_test.write(image_name + ' ' + text + '\n')
                else:
                    f_train.write(image_name + ' ' + text + '\n')
                cnt += 1
    f_train.close()
    f_val.close()
    f_test.close()
    f_image.close()
    
