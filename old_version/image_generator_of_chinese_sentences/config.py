import os
import os.path as osp
import numpy as np
from easydict import EasyDict as edict

__C = edict()
cfg = __C

__C.N = 100
__C.CACHE_DIR = 'cache/'
__C.SAVE_PATH = 'license'
__C.LABEL_PATH = 'license_label.csv'
__C.CORPUS_PATH = 'chars_mapping/Company-Names-Corpus_480W.txt'
__C.PROCESS_NUMBER = 5

__C.FONT_PATH = 'fonts/bank/'
__C.DISTURB_PATH = './disturb/'
__C.BG_PATH = './background/'
__C.HANDWRITING_RATIO = 0

__C.GLOBAL_HEIGHT = 64
__C.BLANK_HEIGHT = 200
__C.BLANK_WIDTH = 4000


__C.RANDOM_CHAR_SPACE = True
__C.RANDOM_EDGE_SPACE = True
__C.RANDOM_TEXT_LINE = True
__C.RANDOM_TEXT_DISTURB = 0.5
__C.RANDOM_REVERSE_COLOR = 0
__C.RANDOM_COLOR_SENTENCE = True
__C.RANDOM_ROTATE_TEXT = 0.2
__C.RANDOM_COLOR_NOISE = True
__C.RANDOM_ERODE = 0.2
__C.RANDOM_DILATE = 0.2
__C.RANDOM_RESIZE = 1.


__C.PRINT = edict()




__C.HANDWRITING = edict()
__C.HANDWRITING.NUM_WRITERS = 240
__C.HANDWRITING.TRAIN_DATA = '/home/zenghuarong/datasets/CASIA_HWDB_change/train'
# __C.HANDWRITING.TRAIN_DATA = ''
__C.HANDWRITING.NUMBERIC = '0123456789'
__C.HANDWRITING.NUMBER_DATA = '/home/zenghuarong/datasets/mnist/train'



def _merge_a_into_b(a, b):
    """Merge config dictionary a into config dictionary b, clobbering the
    options in b whenever they are also specified in a.
    """
    if type(a) is not edict:
        return

    for k, v in a.items():
        # a must specify keys that are in b
        if k not in b:
            raise KeyError('{} is not a valid config key'.format(k))

        # the types must match, too
        old_type = type(b[k])
        if old_type is not type(v):
            if isinstance(b[k], np.ndarray):
                v = np.array(v, dtype=b[k].dtype)
            else:
                raise ValueError(('Type mismatch ({} vs. {}) '
                                'for config key: {}').format(type(b[k]),
                                                            type(v), k))

        # recursively merge dicts
        if type(v) is edict:
            try:
                _merge_a_into_b(a[k], b[k])
            except:
                print(('Error under config key: {}'.format(k)))
                raise
        else:
            b[k] = v

def cfg_from_file(filename):
    """Load a config file and merge it into the default options."""
    import yaml
    with open(filename, 'r') as f:
        yaml_cfg = edict(yaml.load(f))

    _merge_a_into_b(yaml_cfg, __C)

def cfg_from_list(cfg_list):
    """Set config keys via list (e.g., from command line)."""
    from ast import literal_eval
    assert len(cfg_list) % 2 == 0
    for k, v in zip(cfg_list[0::2], cfg_list[1::2]):
        key_list = k.split('.')
        d = __C
        for subkey in key_list[:-1]:
            assert subkey in d
            d = d[subkey]
        subkey = key_list[-1]
        assert subkey in d
        try:
            value = literal_eval(v)
        except:
            # handle the case when v is a string literal
            value = v
        assert type(value) == type(d[subkey]), \
            'type {} does not match original type {}'.format(
            type(value), type(d[subkey]))
        d[subkey] = value
