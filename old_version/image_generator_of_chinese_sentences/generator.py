import re
import os
import cv2
import numpy as np
import threading
from PIL import Image, ImageDraw, ImageFont
from skimage.transform import rotate
import skimage
from keras.preprocessing.image import load_img, img_to_array, array_to_img


def list_pictures(directory, ext='jpg|jpeg|bmp|png|ppm'):
    return [os.path.join(root, f)
            for root, _, files in os.walk(directory) for f in files
            if re.match(r'([\w-]+\.(?:' + ext + '))', f)]


# def load_chars(path=cfg.HANDWRITING.TRAIN_DATA):
#     chars = set(os.listdir(path))
#     chars.update(set(list(NUMBERIC)))
#     return chars

def load_colors_bank():
    magic_number = np.random.random()
    if magic_number < 0.4:
        return tuple(np.array([1,1,1]) * np.random.randint(20,55)), 0
    elif magic_number < 0.6:
        return tuple(np.array([1,1,1]) * np.random.randint(200,255)), 1
    elif magic_number < 0.8:
        return (np.random.randint(200, 255), np.random.randint(30), np.random.randint(30)), 2
    else:
        return (np.random.randint(30), np.random.randint(30), np.random.randint(200, 255)), 3

def is_bg_fit(bg_mean, color_type):
    if bg_mean[0] > 180 and bg_mean[1] > 180 and bg_mean[2] > 180 and color_type == 1:
        return False
    elif bg_mean[0] < 120 and bg_mean[1] < 120 and bg_mean[2] < 120 and (color_type == 0 or color_type == 3):
        return False
    elif bg_mean[0] > 127.5 and bg_mean[1] < 127.5 and bg_mean[2] < 127.5 and color_type == 2:
        return False
    elif bg_mean[0] < 127.5 and bg_mean[1] < 127.5 and bg_mean[2] > 127.5 and color_type == 3:
        return False
    elif np.mean(bg_mean) > 127.5 and color_type == 1:
        return False
    elif np.mean(bg_mean) < 127.5 and color_type == 0:
        return False
    else:
        return True

def choose_bg(bg_files, color_type):
    while True:
        bg_f = bg_files.pop(np.random.randint(len(bg_files)))
        bg_img = img_to_array(load_img(bg_f).convert('RGB'))
        bg_mean = np.mean(np.mean(bg_img,0),0)
        if is_bg_fit(bg_mean, color_type) or len(bg_files) == 0:
            if len(bg_files) == 0:
                print('file nums is 0:', bg_mean)
            break
    return bg_f, bg_files

def load_background_bank(cfg, w, color_type):
    bg_files = list_pictures(cfg.BG_PATH, ext='jpg|png|jpeg')
    bg_f, bg_files = choose_bg(bg_files, color_type)
    bg = img_to_array(load_img(bg_f, target_size=(int(1.2*cfg.GLOBAL_HEIGHT), int(1.2*w))))
    '''
    x = 0
    while x <= w:
        #bg_f = bg_files.pop(np.random.randint(len(bg_files)))
        bg_f, bg_files = choose_bg(bg_files, color_type)
        bg = img_to_array(load_img(bg_f))
        x = bg.shape[1]
        # bug修复：背景中的图像宽度都不够
        if not bg_files:
            bg = img_to_array(load_img(bg_f, target_size=(int(1.2*cfg.GLOBAL_HEIGHT), int(1.2*w))))
            print('no bg files:', np.mean(bg))
            #bg = img_to_array(load_img(os.path.join(cfg.BG_PATH, bg_f), target_size=(int(1.2*cfg.GLOBAL_HEIGHT), int(1.2*w))))
            break
    '''
    while bg.shape[0] <= cfg.GLOBAL_HEIGHT:
        bg_h, bg_w = bg.shape[:2]
        bg = array_to_img(bg).resize((int(bg_w*1.1), int(bg_h*1.1)))
        bg = img_to_array(bg)

    h_pad = np.random.randint(bg.shape[0] - cfg.GLOBAL_HEIGHT)
    w_pad = np.random.randint(bg.shape[1] - w)
    bg = bg[h_pad:h_pad+cfg.GLOBAL_HEIGHT, w_pad:w_pad+w]
    angle = np.random.rand() * 10 - 5
    bg = add_rotate(bg, angle)
    bg = array_to_img(bg).convert('RGBA')

    return bg

def add_rotate(img, angle):
    img = rotate(img, angle, resize=False, mode='reflect')
    return img

def add_noise_all(img, size=20):
    if size <= 20:
        size = 20
    else:
        size = np.random.randint(20, size)
    for i in range(20):
        temp_x = np.random.randint(0, img.shape[0])
        temp_y = np.random.randint(0, img.shape[1])
        temp_r = np.random.randint(0, 255)
        temp_g = np.random.randint(0, 255)
        temp_b = np.random.randint(0, 255)
        img[temp_x][temp_y][0] = temp_r
        img[temp_x][temp_y][1] = temp_g
        img[temp_x][temp_y][2] = temp_b
    return img


def add_erode(img, size):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size, size))
    img = cv2.erode(img, kernel)
    return img

def add_dilate(img, size):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size, size))
    img = cv2.dilate(img, kernel)
    return img

def add_disturb(pil_img, disturb_file):
    img_w, img_h = pil_img.size
    b_data = np.asarray(pil_img.getdata())

    disturb = cv2.imread(disturb_file)
    disturb = cv2.cvtColor(disturb, cv2.COLOR_BGR2GRAY)
    disturb = cv2.bitwise_not(disturb)
    disturb = cv2.adaptiveThreshold(disturb, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 15, 2)
    d_h, d_w = disturb.shape[:2]
    if d_h < img_h + 1:
        disturb = cv2.resize(disturb, (d_w, img_h + 2))
        d_h, d_w = disturb.shape[:2]
    if d_w < img_w + 1:
        disturb = cv2.resize(disturb, (img_w + 2, d_h))
        d_h, d_w = disturb.shape[:2]

    try:
        d_x0, d_y0 = np.random.randint(d_w - img_w), np.random.randint(d_h - img_h)
    except:
        print(d_w, img_w, d_h, img_h)
        assert False, ''
    disturb = disturb[d_y0:d_y0 + img_h, d_x0:d_x0 + img_w]
    arg_index = np.where(b_data.reshape((img_h, img_w, 4))[:, :, 3] == 0)
    disturb[arg_index[0], arg_index[1]] = 0
    b_data[:, 3] = disturb.reshape(-1)
    pil_img.putdata(list(map(tuple, b_data)))

    return pil_img


class data_generate():
    def __init__(self, cfg, corpus, mode='company', cache_dir='cache'):
        self.cfg = cfg
        self.corpus_length = len(corpus)
        self.corpus = corpus
        self.mode = mode
        self.cache_dir = cache_dir
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)

        self.font_paths = list_pictures(self.cfg.FONT_PATH, 'TTF|otf|ttf|ttc')
        self.disturb_file_path = list_pictures(self.cfg.DISTURB_PATH)
        if cfg.HANDWRITING_RATIO != 0:
            self.handwriting_list = os.listdir(self.cfg.HANDWRITING.TRAIN_DATA)

    def augmnet_image(self, pil_img, length):
        img = img_to_array(pil_img)
        if np.random.rand() < self.cfg.RANDOM_REVERSE_COLOR:
            img = np.ones_like(img) - img

        if np.random.random() < self.cfg.RANDOM_ROTATE_TEXT:
            angle = (np.random.rand() * 4 - 2) * 10 / length
            img = add_rotate(img, angle)

        if np.random.random() < self.cfg.RANDOM_COLOR_NOISE:
            img = add_noise_all(img)

        if np.random.random() < self.cfg.RANDOM_ERODE:
            kernel_size = np.random.randint(1, 3)
            img = add_erode(img, kernel_size)

        if np.random.random() < self.cfg.RANDOM_DILATE:
            #kernel_size = np.random.randint(1, 3)
            kernel_size = 1
            img = add_dilate(img, kernel_size)

        if np.random.random() < self.cfg.RANDOM_RESIZE:
            # y = y_min + (y_max-y_min)*(x-x_min)/(x_max-x_min)
            for i in range(np.random.randint(1, 4)):
                ratio = 0.3 + (0.6-0.3)*(np.random.rand()-0)/(1-0)
                h, w = img.shape[:2]
                img = cv2.resize(img, (int(w*ratio), int(h*ratio)))
                img = cv2.resize(img, (w, h))

        img = array_to_img(img)
        return img

    def augment_text(self, pil_img):
        if np.random.random() < self.cfg.RANDOM_TEXT_DISTURB:
            disturb_file = self.disturb_file_path[np.random.randint(len(self.disturb_file_path))]
            pil_img = add_disturb(pil_img, disturb_file)

        return pil_img


    def genetor(self, indexs):
        f_hander = open(os.path.join(self.cache_dir, '%d.txt' % indexs[0]), 'w')

        for ind in indexs:
            if self.mode == 'corpus':
                start = np.random.randint(self.corpus_length - 10)
                sentence = ''.join(self.corpus[start: start + np.random.randint(4, 10)])
            elif self.mode == 'company':
                sentence = self.corpus[ind % self.corpus_length]
            else:
                assert False, 'the mode need to be corpus or company'
            length = len(sentence)

            color, color_type = load_colors_bank()
            #print(ind, sentence)

            blank = Image.new('RGBA', (self.cfg.BLANK_WIDTH, self.cfg.BLANK_HEIGHT), (255, 255, 255, 0))
            current_position = np.random.randint(1, 10)

            if np.random.random() > self.cfg.HANDWRITING_RATIO:
                # 机打
                font_size = np.random.randint(40, 60)
                font_type = self.font_paths[np.random.randint(len(self.font_paths))]
                font = ImageFont.truetype(font_type, font_size)
                # print(ind, font_type, font_size)
                # print(index, '⿺', ImageFont.truetype(font_type, font_size).getoffset('⿺')[1])
                # print(index, '驫', ImageFont.truetype(font_type, font_size).getoffset('驫')[1])
                draw = ImageDraw.Draw(blank)
                for j in range(length):
                    if self.cfg.RANDOM_TEXT_LINE:
                        y = (self.cfg.BLANK_HEIGHT-font_size)//2 + np.random.randint(-3, 3)
                    else:
                        y = (self.cfg.BLANK_HEIGHT - font_size) // 2
                    #########################################
                    font_tmp = font
                    # print(index, font_tmp.getoffset(sentence[j])[1], end='\t')
                    if font_tmp.getoffset('⿺')[1] == font_tmp.getoffset(sentence[j])[1]\
                            or font_tmp.getoffset(sentence[j])[1] > 0.66*font_size:
                        font_tmp = ImageFont.truetype('fonts/bank/ZhongYiSongTi-1.ttf', font_size)
                        # print(os.path.basename(font_paths[np.random.randint(len(font_paths))]))
                    ##########################################
                    draw.text((current_position, y), sentence[j], fill=color, font=font_tmp)
                    if self.cfg.RANDOM_CHAR_SPACE:
                        current_position += draw.textsize(sentence[j], font_tmp)[0] + np.random.randint(-5, 8)
                    else:
                        current_position += draw.textsize(sentence[j], font_tmp)[0]

                max_height = max(draw.textsize(sentence[j], font)) + 6
                # print('', end='\n')

            else:
                # choose a writer
                wter = np.random.randint(self.cfg.HANDWRITING.NUM_WRITERS)

                max_height = 0
                number_ratio = np.random.uniform(1.5, 2.5)
                for j in range(length):
                    char = sentence[j]
                    if char in self.cfg.HANDWRITING.NUMBERIC:
                        char_files = os.listdir(os.path.join(self.cfg.HANDWRITING.NUMBER_DATA, char))
                        char_path = os.path.join(self.cfg.HANDWRITING.NUMBER_DATA, char, char_files[np.random.randint(len(char_files))])
                        char_img = Image.open(char_path)
                        ratio = np.random.uniform(-0.1, 0.1)
                        ratio += number_ratio
                        char_img = char_img.resize((int(char_img.size[0] * ratio), int(char_img.size[1] * ratio)))
                    elif char in self.handwriting_list:
                        char_files = os.listdir(os.path.join(self.cfg.HANDWRITING.TRAIN_DATA, char))
                        for f in char_files:
                            if f.startswith(str(wter)):
                                char_path = os.path.join(self.cfg.HANDWRITING.TRAIN_DATA, char, f)
                                char_img = Image.open(char_path)
                                break

                    else:
                        font_size = np.random.randint(60, 70)
                        char_img = Image.new('RGBA', (70, 70), (255, 255, 255, 0))
                        font = ImageFont.truetype('fonts/bank/ZhongYiSongTi-1.ttf', font_size)
                        draw = ImageDraw.Draw(char_img)
                        draw.text((np.random.randint(1, 5), (70 - font_size) // 2), char, fill=color, font=font)


                    img_data = np.array(char_img.getdata())
                    # print(img_data.shape)
                    alpha = img_data[:, 3]
                    # print(alpha.dtype)
                    # alpha = (alpha * 2).astype(np.int64)
                    alpha = alpha + 100
                    alpha[alpha == 100] = 0

                    new_data = np.bitwise_not((alpha - 255.) == 0.)
                    new_data = new_data.reshape(len(new_data), 1)
                    new_data = np.concatenate([new_data, new_data, new_data], axis=1)
                    new_data = new_data * color
                    datas = np.concatenate([new_data, alpha.reshape(len(alpha), 1)], axis=1)
                    datas = list(map(tuple, datas))
                    char_img.putdata(datas)

                    char_w, char_h = char_img.size

                    if char_h > max_height:
                        max_height = char_h
                    blank.paste(char_img, (current_position, (self.cfg.BLANK_HEIGHT - char_h) // 2), char_img)

                    margin = np.random.randint(1, 10)
                    current_position += char_w + margin

            blank = blank.crop(
                (0, (self.cfg.BLANK_HEIGHT - max_height) // 2, current_position, self.cfg.BLANK_HEIGHT - (self.cfg.BLANK_HEIGHT - max_height) // 2))
            if blank.size[1] >= self.cfg.GLOBAL_HEIGHT:
                ratio = np.random.uniform(0.8, 0.99)
                tmp = self.cfg.GLOBAL_HEIGHT * ratio / blank.size[1]
                blank = blank.resize((int(blank.size[0] * tmp), int(self.cfg.GLOBAL_HEIGHT * ratio)))

            # blank = self.augment_text(blank)
            image_width = blank.size[0] + np.random.randint(2, 100)
            if image_width < self.cfg.GLOBAL_HEIGHT:
                image_width = self.cfg.GLOBAL_HEIGHT
            bg = load_background_bank(self.cfg, image_width, color_type)

            print(ind, sentence, np.mean(np.mean(bg.convert('RGB'),0),0), np.mean(bg.convert('RGB')), color_type)

            w_pad, h_pad = np.array(bg.size) - np.array(blank.size)
            # print(w_pad, h_pad)
            bg.paste(blank, (np.random.randint(w_pad), np.random.randint(h_pad)), blank)
            bg = bg.convert('RGB')
            bg = self.augmnet_image(bg, length)
            file_name = os.path.join(self.cfg.SAVE_PATH, '%.8d.jpg' % ind)
            bg.save(file_name)
            f_hander.write('%s,%s\n' % (os.path.basename(file_name), sentence))
