## Generator of the images of the Chinese Sentences

### introduction

该项目用于生成OCR识别的训练样本，同时支持手写与打印字体的生成。生成手写样本时需要相应的手写字库支持，请联系zenghuarong@4paradigm.com。

### basic tutorial

配置config.py文件

```Python
__C.N = 100	# 生成样本量
__C.CACHE_DIR = 'cache/'	# cache目录，默认即可
__C.SAVE_PATH = 'license'	# 生成样本所在文件夹
__C.LABEL_PATH = 'license_label.csv'	# 生成样本的标签文件
__C.CORPUS_PATH = 'chars_mapping/Company-Names-Corpus_480W.txt'	# 语料文件
__C.PROCESS_NUMBER = 5	# 线程数量
```

执行命令：

Python chineses.py



【20190320】

结合了庆杰的打印字体并做了一些优化，包括：

1. 增加了几个更接近机打票据的字库
2. 随机缩放图片，使视觉效果更接近打印效果
3. 增加文字的印泥效果
4. 随机字符上下左右间距


【20190410】
1.增加白色文字
2.根据文字颜色选择合适背景，避免文字颜色和背景过于相似导致的不清晰的问题
3.背景中的图像宽度都不够时直接resize到合适到宽度（原来程序是换一张图）
4.根据文字长度动态调整图中文字可旋转的最大角度
