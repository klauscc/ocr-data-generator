import os
import pandas as pd
import numpy as np

NUMBERIC = '0123456789'
train_data = '/home/zenghuarong/data/CASIA-HWDB/CASIA_HWDB_change/train'


def load_chars(path=train_data):
    chars = set(os.listdir(path))
    chars.update(set(list(NUMBERIC)))
    return chars

if __name__ == '__main__':
    characters = load_chars()
    characters = list(characters)
    df = pd.DataFrame({'char_index':np.arange(len(characters)), 'char':characters})
    df.to_csv('characters.csv', index=False
              )