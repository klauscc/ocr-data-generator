import os
from keras.preprocessing.image import load_img, array_to_img, img_to_array

image_path = '/Users/harold/github_project/other/BBox-Label-Tool_v1/Images/003/'
label_path = '/Users/harold/github_project/other/BBox-Label-Tool_v1/Labels/003'
dst_path = 'background'

index = 0
for txt_file in os.listdir(label_path):
    with open(os.path.join(label_path, txt_file)) as f:
        text = f.readlines()
    img_file = os.path.join(image_path, txt_file.replace('txt', 'jpg'))
    img = img_to_array(load_img(img_file))
    bbox_num = int(text[0].strip())
    for i in range(bbox_num):
        bbox = text[1+i].strip().split(' ')
        y0, x0, y1, x1 = list(map(int, bbox))
        part = img[x0:x1, y0:y1]
        image_name = os.path.join(dst_path, str(index)+'.png')
        array_to_img(part).save(image_name)
        index += 1
