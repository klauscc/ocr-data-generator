import codecs
import csv
import os
import shutil
from datetime import datetime
import numpy as np
from multiprocessing import Pool

from generator import data_generate
# np.random.seed(123456)
from config import cfg, cfg_from_file

def cangjie_render(cfg):
    with codecs.open(cfg.CORPUS_PATH, 'r', 'utf-8') as f:
        company_name = f.readlines()
        company_name = list(map(lambda x:x.strip(), company_name))
    company_name = np.random.choice(company_name, cfg.N)

    if not os.path.exists(cfg.SAVE_PATH):
        os.makedirs(cfg.SAVE_PATH)
    if os.path.exists(cfg.CACHE_DIR):
        shutil.rmtree(cfg.CACHE_DIR, ignore_errors=True)

    last = 0
    if os.path.exists(cfg.LABEL_PATH):
        with open(cfg.LABEL_PATH, 'r') as f:
            context = f.readlines()
            if context:
                last = context[-1]
                last = last.strip().split(',')[0].split('.')[0]
                last = int(last) + 1

    all_indexs = [list(range(last+i, cfg.N, cfg.PROCESS_NUMBER)) for i in range(cfg.PROCESS_NUMBER)]
    gen = data_generate(cfg, company_name, mode='company', cache_dir=cfg.CACHE_DIR)
    p = Pool(cfg.PROCESS_NUMBER)
    p.map(gen.genetor, all_indexs)
    # for i in range(last, cfg.N + last):
    #     img, sentence = next(gen)
    #     file_name = os.path.join(cfg.SAVE_PATH, '%.8d.jpg' % i)
    #     img.save(file_name)
    #     writer.writerow([os.path.basename(file_name), sentence])
    p.close()
    f_handler = open(cfg.LABEL_PATH, 'a')
    cache_files = sorted(os.listdir(cfg.CACHE_DIR))
    h_handlers = [open(os.path.join(cfg.CACHE_DIR, f), 'r') for f in cache_files]
    while True:
        for handler in h_handlers:
            try:
                line = handler.readline()
                f_handler.write(line)
            except:
                return

def main():
    cfg_from_file('text.yml')
    cangjie_render(cfg)

if __name__=='__main__':
    main()



    