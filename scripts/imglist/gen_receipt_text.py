# -*- coding: utf-8 -*-
#================================================================
#   God Bless You. 
#   
#   file name: gen_receipt_text.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/07/12
#   description: 
#
#================================================================

import os
import sys
import numpy as np
import glob
import html
import xml.etree.ElementTree as ET

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

MATEARIAL_DIR = os.path.join(CURRENT_FILE_DIRECTORY, "../data/material")
