# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: gen_num.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/06/11
#   description:
#
#================================================================
import os
import sys
import string
import numpy as np
from datetime import datetime

RAND_FN = np.random.rand


def gen_number(min_len=1, max_len=10, random_add_zero=False, max_point_len=3):
    """generate number

    Kwargs:
        min_len (TODO): TODO
        max_len (TODO): TODO
        random_add_zero:  If True. randomly add zero before the number.
        max_point_len: The digit count after point.

    Returns: TODO

    """
    num_len = np.random.randint(min_len, max_len + 1)
    number = ""

    for i in range(num_len):
        number += str(np.random.randint(0, 10))

    #point
    if max_point_len > 0 and np.random.rand() < 0.5:
        number += "."
        point_len = np.random.randint(1, max_point_len + 1)
        for i in range(point_len):
            number += str(np.random.randint(0, 10))

    if RAND_FN() < 0.3:
        number = "-" + number    # negative
    else:
        if random_add_zero and RAND_FN() < 0.1:
            i = np.random.randint(1, 4)
            number = "0" * i + number

    float_number = float(number)
    if RAND_FN() < 0.5:
        float_number = int(float_number)

    if RAND_FN() < 0.5:
        f = "{:,}"
    else:
        f = "{}"
    return f.format(float_number)


pre_defined_units = [
    "kg", "g", "ton", "mg", "mol", "l", "ml", "m", "ms", "s", "h", "min", "d",
    "day", "U",
    ] # yapf: disable
alpha = list(string.ascii_lowercase)


def gen_num_unit(min_len=1, max_len=8):
    """ Format: [number] [unit](/[unit]). e.g. 10^3 mol/l, 10kg, 10.32KG/m^3

    Kwargs:
        min_len (TODO): TODO
        max_len (TODO): TODO

    Returns: TODO

    """

    def gen_unit():
        rnd = RAND_FN()
        if rnd < 0.3:
            unit = np.random.choice(pre_defined_units)
        else:
            unit = "".join(list(np.random.choice(alpha, size=np.random.randint(1, 4))))
        if RAND_FN() < 0.5:
            unit = unit.upper()
        if RAND_FN() < 0.2:
            unit += "^" + str(np.random.randint(1, 4))
        return unit

    if RAND_FN() < 0.2:
        number = gen_number(1, 3, max_point_len=2) + "^" + gen_number(1, 4, max_point_len=0)
    else:
        number = gen_number(min_len, max_len, max_point_len=4, random_add_zero=False)

    if RAND_FN() < 0.4:
        unit = gen_unit() + "/" + gen_unit()
    else:
        unit = gen_unit()
    return number + unit


time_stamp = 1.0e11


def gen_time():
    """generate time
    Returns: TODO

    """
    t = np.random.randint(0, time_stamp)
    date_time = datetime.fromtimestamp(t)
    if RAND_FN() < 0.5:
        time = date_time.strftime("%H:%M:%S")
    else:    #date
        if RAND_FN() < 0.5:
            fmt = "%m/%d/%Y"
        else:
            fmt = "%Y-%m-%d"
        time = date_time.strftime(fmt)
    return time


def main():
    NUM = 100000

    prefix = ["number", "num_unit", "time"]
    fn = [gen_number, gen_num_unit, gen_time]

    save_path = sys.argv[1]
    with open(save_path, "w") as f:
        for i in range(len(prefix)):
            fmt = prefix[i] + "_{:06d}.jpg {}"
            for j in range(NUM):
                label = fn[i]()
                line = fmt.format(j, label)
                f.write(line + "\n")


if __name__ == "__main__":
    main()
