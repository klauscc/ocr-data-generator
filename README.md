# ocr-data-synthesis

准备通用印刷体的训练数据

## 1. Dependencies

- Pillow
- Opencv
- colormath
- imgaug

Install structions:
```
pip install opencv-contrib-python==3.4.2.17
pip install colormath==3.0.0
pip install imgaug==0.2.9
```

**Before running any scripts, add `data_synthesis` to PYTHONPATH by**:

```
source prepare_env.sh
```

## 2. Code Structure

```
|-- charset         charsets appeared in the label files
|-- old version     old data synthesis codes
|-- scripts         scripts for data generating
|-- data_synthesis  core synthesis code
    |-- utils           utils such as label replacement
    |-- generator.py    ocr image generate 
    |-- templates.py    parameter templates which controll the synthesis
    |-- text_generator.py   Generators for Fonts, Colors, Backgrounds, Texts
    |-- image_writer.py     Write text on images.
    |-- augmenter.py        Augmentation on text-image and background-image.
```

## 3. Usage

### 3.1 首先切换到脚本目录
```
cd scripts
```

### 3.2 生成图片名和对应要写的文字的txt.

```
python gen_imglist.py
```
路径和文件名等可能需要修改，里面一个函数是一个类型的图片的细节.

生成txt示例:
<pre>
receipt_00000000.jpg 上海市闸北区恒丰北路100号2
receipt_00000001.jpg 北京市平谷区大华山镇大华山大街269号
receipt_00000002.jpg 淮安市北京新村9区7幢102室96893367
receipt_00000003.jpg 上海市浦东新区川沙路4
receipt_00000004.jpg 尉氏县蔡庄镇南街村356979135
receipt_00000005.jpg 乐亭县姜各庄镇孙坨
receipt_00000006.jpg 江门市东华一路37号购书中心新华电脑城二楼212铺6574860439271
receipt_00000007.jpg 河北省沧州市新华区长芦大道彩龙国际商贸广场A-
receipt_00000008.jpg 潍坊市坊子区长宁街63号（原
receipt_00000009.jpg 巴彦县巴彦镇红星村
</pre>

### 3.3 根据第二步生成的txt生成图片

```
python gen_img.py
```

生成的时候使用了`data_synthesis/templates.py`里面预定义的一些参数，可以根据实际需求更改.


### 3.4 生成label文件

```
python gen_label.py
```

这一步是结合第2步生成的txt和第3步生成的图片生成label.
第2步的txt中的文字需要使用词典进行替换(全角->半角等),然后为生成的图片一一生成对应的label到一个新的txt文件中.


## 4. Code Logic
写一张图片到背景上(image_writer.ImageWriter.write)的流程如下:

1. **生成**要写的**文字**(text_generator.TextGenerator)
2. **生成**用于写每个字的**字体**(字体种类、大小) (text_generator.TextGenerator)
3. **计算**需要写的**字**的每一个的**h和w**
4. 随机生成每个**字之间的x间距和y间距**
5. 随机**挑选背景**(text_generator.BackgroundGenerator)，并且根据要写的字占据的大小重复补齐背景(pad with repeat)
6. 根据背景平均颜色**挑选**写每个字的**颜色**，保证字体和背景颜色差异肉眼可分(text_generator.ColorGenerator)
7. **生成透明text-image**,在上面写字,并进行增广(透视变换、腐蚀字本身,augmenter.TextAugmenter)
8. **将text-image贴在bg-image上**，并随机增广(augmenter.ImageAugmenter)
9. 在贴好的图片上对**文字区域进行裁剪**(augmenter.Cropper)

调用链:
`scripts/gen_img.py` -> `data_synthesis/generator.py` -> `data_synthesis/image_writer.py`Code Logic
