# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: common.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/05/05
#   description:
#
#================================================================


def truncx(x, min_val, max_val):
    if x < min_val:
        return min_val
    elif x > max_val:
        return max_val
    else:
        return x
