# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: random_factory.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/06/27
#   description:
#
#================================================================

import numpy as np


class RandomFactory(object):
    """generate random functions controllable by seed

    Args:
        seed: Int. The seed to controll all the random states.

    """

    def __init__(self, seed=None):
        """TODO: to be defined1. """
        self.seed = seed
        self.seedForRandomStates()
        self.current_allocate_idx = 0
        self.random_states = []

    def seedForRandomStates(self, n=1000):
        self.randomstate_fn = np.random.RandomState(seed=self.seed)
        self.randomstate_seeds = self.randomstate_fn.randint(low=10, high=100000, size=n)

    def get_random_state(self, idx=None):
        """get or create a new random_state

        Kwargs:
            idx (TODO): TODO

        Returns: TODO

        """
        if idx is None:
            random_state = np.random.RandomState(
                seed=self.randomstate_seeds[self.current_allocate_idx])
            self.current_allocate_idx += 1
            self.random_states.append(random_state)
            return random_state

        if idx >= self.current_allocate_idx:
            raise ValueError("could not get RandomState of {}: idx out of rangeb".format(idx))
        else:
            return self.random_states[idx]
