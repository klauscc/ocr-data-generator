# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: test_tfdataset.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/06/27
#   description:
#
#================================================================
import os
import time
import tensorflow as tf

import templates
from templates import PARAM_SETS
from generator import ImageGenerator

os.environ['CUDA_VISIBLE_DEVICES'] = ""

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

param = templates.BASIC
text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../../data/texts/qingjie80W/qingjie80W_0.txt")
param["text_file_to_write"] = text_file
param["save_dir"] = ""
gen = ImageGenerator(param)

# dataset = gen.tf_dataset()
pd_ds = gen.tf_dataset_parallel(n_worker=8)

eval_ds = [pd_ds] 

with tf.Session() as sess:
    with tf.device("/cpu:0"):
        for ds in eval_ds:
            print("ds: {}".format(ds))
            iterator = ds.make_one_shot_iterator()
            image, label = iterator.get_next()
            ave_time = 0
            for i in range(100000):
                t1 = time.time()
                img, l = sess.run([image, label])
                t2 = time.time()
                t = t2 - t1
                ave_time += (t - ave_time) / (i + 1)
                if i % 100 == 0:
                    print("ave_time: {:.3f}s. samples per second: {}".format(
                        ave_time, int(1 / ave_time)))
