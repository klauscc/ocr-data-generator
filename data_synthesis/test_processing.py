# -*- coding: utf-8 -*-
#================================================================
#   God Bless You.
#
#   file name: test_processing.py
#   author: klaus
#   email: klaus.cheng@qq.com
#   created date: 2019/06/27
#   description:
#
#================================================================

import os
from multiprocessing import Process, Queue

CURRENT_FILE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))

from generator import gen
import templates



param = templates.BASIC
text_file = os.path.join(CURRENT_FILE_DIRECTORY, "../data/texts/qingjie80W/qingjie80W_0.txt")
param["text_file_to_write"] = text_file
param["save_dir"] = ""

if __name__ == '__main__':
    q = Queue(10)
    p = Process(target=gen, args=(param, q))
    p.start()
    print(q.get())    # prints "[42, None, 'hello']"
    p.join()
